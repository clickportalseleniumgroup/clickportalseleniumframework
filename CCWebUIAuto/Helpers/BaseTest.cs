﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using CCWebUIAuto.PrimitiveElements;
using CommonUtilities;
using Microsoft.Expression.Encoder.ScreenCapture;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CCWebUIAuto.Helpers
{
  public class BaseTest
    {
        // create a user to login as for the test
        private ScreenCaptureJob scj;
        public string _testScreenCaptureFileName = string.Empty;

        [SetUp]
        public void Setup()
        {
            const String pathname = @"..\..\autoConfig.xml";
            try
            {
               ClickPortalUI.AutoConfig.read(pathname);
               // Create necessary log directories if necessary
                string debugVideoRecordingsDir = ClickPortalUI.AutoConfig["DebugLogLocation"] + @"\VideoRecordings\";

                if (!Directory.Exists(debugVideoRecordingsDir))
                {
                    Directory.CreateDirectory(debugVideoRecordingsDir);
                }
                if (ClickPortalUI.AutoConfig.ContainsKey("EnableVideoRecording") && ClickPortalUI.AutoConfig["EnableVideoRecording"].ToLower() == "true")
                {
                    // need to clean test name in case unit test appends additional information to name
                    string cleansedTestName = Regex.Replace(TestContext.CurrentContext.Test.FullName, @"\(""*.*""\)", "");
                    _testScreenCaptureFileName = debugVideoRecordingsDir + cleansedTestName + ".wmv";
                    if (File.Exists(_testScreenCaptureFileName))
                    {
                        File.Delete(_testScreenCaptureFileName);
                    }
                    scj = new ScreenCaptureJob();
                    scj.OutputScreenCaptureFileName = _testScreenCaptureFileName;
                    scj.Start();
                    Trace.WriteLine(String.Format("Starting recording for test: {0}", TestContext.CurrentContext.Test.FullName));
                }
                ClickPortalUI.Initialize();
                Trace.WriteLine(String.Format("Executing test: {0}", TestContext.CurrentContext.Test.FullName));
            }
            catch (Exception e)
            {
                ExceptionHandler.HandleException(e);
            }
        }

        [TearDown]
        public void TearDown()
        {
            bool deleteRecording = true;

            try
            {
            
                if (TestContext.CurrentContext.Result.Status == TestStatus.Failed ||
                    TestContext.CurrentContext.Result.Status == TestStatus.Inconclusive)
                {
                    deleteRecording = false;
                    // Want to pause to view problem on video recording before shutting down browser
                    Thread.Sleep(3000);
                }
            }
            catch (Exception ex)
            {
				Web.HandleException(ex);
            }
            finally
            {
                Trace.WriteLine("Closing browser.");
				Web.Driver.Quit();
                if (ClickPortalUI.AutoConfig.ContainsKey("EnableVideoRecording") && ClickPortalUI.AutoConfig["EnableVideoRecording"].ToLower() == "true")
                {
                    scj.Stop();
                    scj.Dispose();
                    if (deleteRecording)
                    {
                        Trace.WriteLine("Removing unnecessary test recording:  " + _testScreenCaptureFileName);
                        File.Delete(_testScreenCaptureFileName);
                    }
                }
            }
        }

		public Button Button(By locator)
		{
			return new Button(locator);
		}

		public Link Link(By locator)
		{
			return new Link(locator);
		}

		public TextBox TextBox(By locator)
		{
			return new TextBox(locator);
		}

		public void AcceptAlert()
		{
			RetriableRunner.Run(() => Web.Driver.SwitchTo().Alert().Accept());
		}

		public void AcceptAlert(String alertText)
		{
			RetriableRunner.Run(() => {
				var alert = Web.Driver.SwitchTo().Alert();
				Assert.AreEqual(alertText, alert.Text);
				alert.Accept();	
			});
		}

		public void SwitchToMostRecentWindow()
		{
			var handles = Web.Driver.WindowHandles;
			var handle = handles.Last();
			Web.Driver.SwitchTo().Window(handle);
		}
    }
}
